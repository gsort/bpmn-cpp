#!/bin/bash

find src/ -name "*.h" -o -name "*.cpp" | xargs clang-tidy --quiet -p build/compile_commands.json --export-fixes=fixes.yml 2>&1 >/dev/null
if [ -f "fixes.yml" ]; then
    scripts/analyzers/clangfixes2cc.py fixes.yml > reports/gl-code-quality-report-clang-tidy.json
else
    echo "[]" > reports/gl-code-quality-report-clang-tidy.json
fi
rm -rf fixes.yml
