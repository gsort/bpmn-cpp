#!/bin/bash

cd build
cmake --build . --target clean
cmake --build . | sed -n '/Warning: Unused direct dependencies:/,/^$/p' > ../lwyu.txt
cd ../
if [ `cat lwyu.txt | wc -l` -gt 0 ]; then
    scripts/analyzers/lwyu2cc.py lwyu.txt > reports/gl-code-quality-report-lwyu.json
else
    echo "[]" > reports/gl-code-quality-report-lwyu.json
fi
rm -rf lwyu.txt
