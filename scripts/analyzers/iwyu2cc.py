#!/usr/bin/python3

import sys
import json
import re
import hashlib
import os

if (len(sys.argv) < 2):
    print ('Usage: iwyu2cc.py <iwyu-output-file>')
    sys.exit(1)

contents = open(sys.argv[1], 'r').read()
p = re.compile('(.*?) should remove these lines:\n((- #include[^\n]*?\n)+)')

output = []

currentDir = os.getcwd() + '/'
for m in re.finditer(p, contents):
    for i in re.finditer(r'- (.*?)(\n|$)', m.group(2)):
        o = {
            'description': 'iwyu [' + m.group(1).replace(currentDir, '') + '] should remove the ' + i.group(1),
            'fingerprint': hashlib.sha256((m.group(1)+i.group(1)).encode('utf-8')).hexdigest(),
            'severity': 'minor',
            'location': {
                'path': m.group(1).replace(currentDir, '')
            }
        }
        output.append(o)

print(json.dumps(output, indent=4))
