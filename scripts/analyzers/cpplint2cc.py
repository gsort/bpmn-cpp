#!/usr/bin/python3

import sys
import json
import re
import hashlib

if (len(sys.argv) < 2):
    print ('Usage: cpplint2cc.py <cpplint-output-file>')
    sys.exit(1)

lines = open(sys.argv[1], 'r').readlines()
p = re.compile('^(.*?):([0-9]*?|None):\s*(.*?)\s*\[(.*?)\] \[[0-9]\]')

output = []

for line in lines:
    m = p.match(line)
    o = {
        'description': 'cpplint [' + m.group(4) + ']: ' + m.group(3),
        'fingerprint': hashlib.sha256((m.group(1)+m.group(2)).encode('utf-8')).hexdigest(),
        'severity': 'minor',
        'location': {
            'path': m.group(1),
            'lines': {
                'begin': m.group(2)
            }
        }
    }
    output.append(o)

print(json.dumps(output, indent=4))
