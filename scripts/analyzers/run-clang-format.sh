#!/bin/bash

find src/ -name "*.h" -o -name "*.cpp" | xargs clang-format --dry-run --Werror 2> clang-format.txt
if [ `cat clang-format.txt | wc -l` -gt 0 ]; then
    scripts/analyzers/clangformat2cc.py clang-format.txt > reports/gl-code-quality-report-clang-format.json
else
    echo "[]" > reports/gl-code-quality-report-clang-format.json
fi
rm -rf clang-format.txt
