#!/usr/bin/python3

import sys
import os
import yaml
import json
import hashlib

from yaml import Loader

if (len(sys.argv) < 2):
    print ('Usage: clangfixes2cc.py <input-clang-fixes.yml>')
    sys.exit(1)

stream = open(sys.argv[1], 'r')
input = yaml.load(stream, Loader=Loader)
output = []

currentDir = os.getcwd() + '/'
for d in input['Diagnostics']:
    source = open(d['DiagnosticMessage']['FilePath'], 'r').read()
    lineNumber = source[:d['DiagnosticMessage']['FileOffset']].count('\n')+1
    o = {
        'description': 'clang-tidy [' + d['DiagnosticName'] + ']: ' + d['DiagnosticMessage']['Message'],
        'fingerprint': hashlib.sha256((d['DiagnosticMessage']['FilePath']+str(d['DiagnosticMessage']['FileOffset'])).encode('utf-8')).hexdigest(),
        'severity': 'critical' if (d['Level'] == 'Error') else 'minor',
        'location': {
            'path': d['DiagnosticMessage']['FilePath'].replace(currentDir, ''),
            'lines': {
                'begin': lineNumber
            }
        }
    }
    output.append(o)

print(json.dumps(output, indent=4))
