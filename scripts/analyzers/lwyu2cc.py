#!/usr/bin/python3

import sys
import json
import re
import hashlib

if (len(sys.argv) < 2):
    print ('Usage: lwyu2cc.py <lwyu-output-file>')
    sys.exit(1)

lines = open(sys.argv[1], 'r').readlines()
del lines[0]

output = []

for line in lines:
    o = {
        'description': 'lwyu: unneeded link to library ' + line.strip(),
        'fingerprint': hashlib.sha256(line.strip().encode('utf-8')).hexdigest(),
        'severity': 'minor',
    }
    output.append(o)

print(json.dumps(output, indent=4))
