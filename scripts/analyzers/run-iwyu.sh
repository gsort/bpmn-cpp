#!/bin/bash

find src/ -name "*.h" -o -name "*.cpp" | \
     xargs -I {} /opt/iwyu/bin/iwyu_tool.py -p build/ {} -- -Xiwyu -cxx17ns 2>/dev/null > iwyu-full.txt
cat iwyu-full.txt | sed -n '/should remove these lines/,/^$/p' > iwyu.txt
if [ `cat iwyu.txt | wc -l` -gt 1 ]; then
    scripts/analyzers/iwyu2cc.py iwyu.txt > reports/gl-code-quality-report-iwyu.json
else
    echo "[]" > reports/gl-code-quality-report-iwyu.json
fi
rm -rf iwyu.txt
