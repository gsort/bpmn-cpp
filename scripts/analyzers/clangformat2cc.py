#!/usr/bin/python3

import sys
import json
import re
import hashlib
import os

if (len(sys.argv) < 2):
    print ('Usage: clangformat2cc.py <clang-format-output-file>')
    sys.exit(1)

contents = open(sys.argv[1], 'r').read()
p = re.compile('(.*?):(\d*):(\d*): error: code should be clang-formatted \[-Wclang-format-violations\]\n(.*)')

output = []

currentDir = os.getcwd() + '/'
for m in re.finditer(p, contents):
    o = {
        'description': 'clang-format [clang-format-violations]: ' + m.group(4),
        'fingerprint': hashlib.sha256((m.group(4)).encode('utf-8')).hexdigest(),
        'severity': 'minor',
        'location': {
            'path': m.group(1).replace(currentDir, ''),
            'lines': {
                'begin': m.group(2)
            }
        }
    }
    output.append(o)

print(json.dumps(output, indent=4))
