#!/bin/bash

find src/ -name "*.h" -o -name "*.cpp" | xargs cpplint --quiet 2> cpplint.txt
if [ -f "cpplint.txt" ]; then
    scripts/analyzers/cpplint2cc.py cpplint.txt > reports/gl-code-quality-report-cpplint.json
else
    echo "[]" > reports/gl-code-quality-report-cpplint.json
fi
rm -rf cpplint.txt
