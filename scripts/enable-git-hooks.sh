#!/bin/bash

cd .git/hooks
ln -s ../../hooks/clang-format-pre-commit.hook pre-commit
cd ../../
