#!/bin/bash

mkdir -p reports
for analyzer in `dirname "$0"`/analyzers/*.sh; do
  bash "$analyzer" -H 
done
jq -n "[inputs] | add" reports/*.json > final-quality-report.json
rm -rf reports
