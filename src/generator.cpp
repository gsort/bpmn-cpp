/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "generator.h"

#include <spdlog/spdlog.h>

#include <filesystem>
#include <fstream>
#include <memory>
#include <vector>

#include <boost/algorithm/string/case_conv.hpp>
#include <cxxopts.hpp>

#include "classbasicvisitor.h"
#include "enumbasicvisitor.h"
#include "propertiescollectorvisitor.h"

namespace fs = std::filesystem;

Generator::Generator(const cxxopts::ParseResult &cxxOptions)
    : _cxxOptions(cxxOptions), _errorCounter(0)
{
}

auto Generator::generate() -> bool
{
   if (!init()) {
      return false;
   }

   invokeVisitors();

   if (!prepareTemplates()) {
      return false;
   }

   if (!generateCPP()) {
      return false;
   }

   // Handle errors
   if (_errorCounter == 0U) {
      spdlog::info("All C++ class metamodels generated successfully");
   } else {
      spdlog::warn(fmt::format("Some C++ class metamodels were generated but {} errors were "
                               "detected, please check the logs out",
                               _errorCounter));
   }

   return true;
}

auto Generator::init() -> bool
{
   // Handle output folder
   _outputFolder = _cxxOptions["output"].as<std::string>();
   fs::path path(_outputFolder);
   fs::create_directories(_outputFolder);

   // Load BPMN cmof file
   auto presult = _doc.load_file(_cxxOptions["input"].as<std::string>().data());
   if (!presult) {
      spdlog::critical("Error when parsing XML file {}", _cxxOptions["input"].as<std::string>());
      return false;
   }

   return true;
}

void Generator::invokeVisitors()
{
   _data = inja::json::array();

   // Create visitors
   std::vector<std::shared_ptr<ICmofVisitor> > visitors{
      std::make_shared<ClassBasicVisitor>(), std::make_shared<PropertiesCollectorVisitor>(),
      std::make_shared<EnumBasicVisitor>()
   };

   // Load metaenums and invoker visitors
   auto metaenums
       = _doc.select_nodes("/xmi:XMI/cmof:Package/ownedMember[@xmi:type='cmof:Enumeration']");

   for (const auto &metaEnum : metaenums) {
      for (const auto &visitor : visitors) {
         visitor->visitEnumeration(metaEnum.node(), _data);
      }
   }

   // Load metaclasses and invoke visitors
   auto metaclasses
       = _doc.select_nodes("/xmi:XMI/cmof:Package/ownedMember[@xmi:type='cmof:Class']");

   for (const auto &metaClass : metaclasses) {
      for (const auto &visitor : visitors) {
         visitor->visitClass(metaClass.node(), _data);
      }
   }
}

auto Generator::prepareTemplates() -> bool
{
   // Add callbacks

    _env.add_callback("capitalize", 1, [](inja::Arguments args) {
      std::string str = args.at(0)->get<std::string>();
      str[0] = std::toupper(str[0]);
      return str;
   });

   // Parse templates
   try {
      _tmplHeader = _env.parse_template("../templates/header.tmpl");
      _tmplCpp = _env.parse_template("../templates/cpp.tmpl");
      _tmplEnum = _env.parse_template("../templates/enum.tmpl");
   } catch (const inja::FileError &ferror) {
      spdlog::critical(ferror.what());
      return false;
   } catch (const inja::ParserError &perror) {
      spdlog::error(perror.what());
      return false;
   } catch (const inja::InjaError &ierror) {
      spdlog::error(ierror.what());
      return false;
   }

   return true;
}

auto Generator::generateCPP() -> bool
{
   std::ofstream out_metaheader_file;
   std::ofstream out_metacpp_file;

   // Generate metaclasses and metaenums
   for (const auto &jsonObj : _data) {
      bool is_enum = false;
      std::string enumNameLower;
      std::string classNameLower;

      if ((jsonObj.find("class_name")) == jsonObj.end()) {
         if ((jsonObj.find("enum_name")) == jsonObj.end()) {
            continue;
         }
         is_enum = true;
         enumNameLower = jsonObj["enum_name"];
      } else {
         classNameLower = jsonObj["class_name"];
      }

      if (_cxxOptions["verbose"].as<bool>()) {
         spdlog::info(fmt::format("Generating C++ {} {} metamodel at {}",
                                  (is_enum) ? "enum" : "class",
                                  (is_enum) ? enumNameLower : classNameLower, _outputFolder));
      }

      if (!is_enum) {
         out_metaheader_file.open(fmt::format("{}/{}.h", _outputFolder,
                                              boost::algorithm::to_lower_copy(classNameLower)));
         out_metacpp_file.open(fmt::format("{}/{}.cpp", _outputFolder,
                                           boost::algorithm::to_lower_copy(classNameLower)));
      } else {
         out_metaheader_file.open(
             fmt::format("{}/{}.h", _outputFolder, boost::algorithm::to_lower_copy(enumNameLower)));
      }

      try {
         if (!is_enum
             && (_env.render_to(out_metaheader_file, _tmplHeader, jsonObj).fail()
                 || _env.render_to(out_metacpp_file, _tmplCpp, jsonObj).fail())) {
            spdlog::error(fmt::format("Unexpected error while generating metamodel of C++ class {}",
                                      jsonObj["class_name"]));
            ++_errorCounter;
         } else if (is_enum && _env.render_to(out_metaheader_file, _tmplEnum, jsonObj).fail()) {
            spdlog::error(fmt::format("Unexpected error while generating metamodel of C++ enum {}",
                                      jsonObj["enum_name"]));
            ++_errorCounter;
         }
      } catch (const inja::RenderError &rerror) {
         spdlog::critical(rerror.what());
         return false;
      }

      out_metaheader_file.close();
      out_metacpp_file.close();
   }
   return true;
}
