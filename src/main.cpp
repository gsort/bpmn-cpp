/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include <spdlog/spdlog.h>

#include <cxxopts.hpp>

#include "generator.h"

auto main(int argc, char **argv) -> int  // NOLINT(bugprone-exception-escape)
{
   // NOLINT(whitespace/braces)
   // Handle command-line arguments
   cxxopts::Options options("bpmn-cpp", "A Modern C++ BMPN metamodel implementation generator");
   // clang-format off
   options.add_options()
         ("h,help", "Print usage")
         ("i,input", "Input .cmof BPMN file", cxxopts::value<std::string>())
         ("o,output", "Destination folder", cxxopts::value<std::string>()->default_value("./"))
         ("v,verbose", "Verbose output", cxxopts::value<bool>()->default_value("false"))
         ;  // NOLINT(whitespace/semicolon)
   // clang-format on

   auto cxx_options = options.parse(argc, argv);

   if (argc == 1 || cxx_options.count("help") != 0) {
      spdlog::set_pattern("%v");
      spdlog::info(options.help());
      exit(0);
   }

   Generator generator{ cxx_options };
   if (!generator.generate()) {
      spdlog::critical("Error when running generator");
   }

   return 0;
}
