/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "classbasicvisitor.h"

#include <string>

#include <nlohmann/json.hpp>
#include <pugixml.hpp>

void ClassBasicVisitor::visitClass(const pugi::xml_node &classNode, inja::json &data)
{
   std::istringstream baseClass_extractor;
   inja::json jsonClass = { { "class_name", classNode.attribute("name").value() } };
   jsonClass["base_class"] = inja::json::array();
   baseClass_extractor.str(classNode.attribute("superClass").value());
   std::string baseClass_name;

   while (baseClass_extractor >> baseClass_name) {
      jsonClass["base_class"].push_back(baseClass_name);
   }

   baseClass_extractor.clear();
   data.push_back(jsonClass);
}
