/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef GENERATOR_H_
#define GENERATOR_H_

#include <string>
#include <vector>

#include <inja/environment.hpp>
#include <inja/template.hpp>
#include <pugixml.hpp>

namespace cxxopts
{
class ParseResult;
}  // namespace cxxopts

class Generator
{
public:
   explicit Generator(const cxxopts::ParseResult &cxxOptions);

   [[nodiscard]] auto generate() -> bool;

private:
   auto init() -> bool;
   void invokeVisitors();
   auto prepareTemplates() -> bool;
   auto generateCPP() -> bool;

   const cxxopts::ParseResult &_cxxOptions;
   pugi::xml_document _doc;
   std::string _outputFolder;
   std::vector<std::string> _enumVector;
   inja::json _data;
   inja::Environment _env;
   inja::Template _tmplHeader;
   inja::Template _tmplCpp;
   inja::Template _tmplEnum;
   unsigned _errorCounter;
};

#endif  // GENERATOR_H_
