/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef CLASSBASICVISITOR_H_
#define CLASSBASICVISITOR_H_

#include "icmofvisitor.h"

class ClassBasicVisitor : public ICmofVisitor
{
public:
   ClassBasicVisitor() = default;

   void visitClass(const pugi::xml_node &classNode, inja::json &data) override;
};

#endif  // CLASSBASICVISITOR_H_
