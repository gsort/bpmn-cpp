/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#ifndef ICMOFVISITOR_H_
#define ICMOFVISITOR_H_

#include <inja/environment.hpp>

namespace pugi
{
class xml_node;
}  // NOLINT(llvm-namespace-comment, readability/namespace)

class ICmofVisitor
{
public:
   virtual void visitClass(const pugi::xml_node &classNode, inja::json &data);
   virtual void visitEnumeration(const pugi::xml_node &enumerationNode, inja::json &data);
   virtual void visitAssociation(const pugi::xml_node &associationNode, inja::json &data);

protected:
   ICmofVisitor() = default;
};

#endif  // ICMOFVISITOR_H_
