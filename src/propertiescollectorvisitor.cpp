/***************************************************************************
**
**  Copyright (C) 2020 by Sandro Andrade <sandroandrade@kde.org>
**
**  This program is free software: you can redistribute it and/or modify
**  it under the terms of the GNU General Public License as published by
**  the Free Software Foundation, either version 3 of the License, or
**  (at your option) any later version.
**
**  This program is distributed in the hope that it will be useful,
**  but WITHOUT ANY WARRANTY; without even the implied warranty of
**  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
**  GNU General Public License for more details.
**
**  You should have received a copy of the GNU General Public License
**  along with Foobar. If not, see <https://www.gnu.org/licenses/>.
**
***************************************************************************/

#include "propertiescollectorvisitor.h"

#include <algorithm>
#include <iterator>
#include <set>
#include <string>
#include <vector>

#include <boost/algorithm/string.hpp>
#include <nlohmann/json.hpp>
#include <pugixml.hpp>

void PropertiesCollectorVisitor::visitClass(const pugi::xml_node &classNode, inja::json &data)
{
   auto metaProperties = classNode.children("ownedAttribute");

   inja::json jsonProperties = inja::json::array();
   inja::json jsonCppHeaders = inja::json::array();
   inja::json jsonForwardDcls = inja::json::array();
   inja::json jsonForwardEnumDcls = inja::json::array();

   std::set<std::string> rawCppHeaders;
   std::set<std::string> rawForwardDcls;
   std::set<std::string> rawForwardEnumDcls;
   std::set<std::string> rawBaseClasses;

   for (const auto &metaProperty : metaProperties) {
      std::string prop_name{ metaProperty.attribute("name").value() };
      std::string prop_type{ metaProperty.attribute("type").value() };
      std::string prop_cardinality{ metaProperty.attribute("upper").value() };
      std::string prop_composite{ metaProperty.attribute("isComposite").value() };
      bool is_primitive{ false };

      if (prop_type.empty()) {
         auto type_tag = metaProperty.child("type");

         auto attribute = type_tag.attribute("href");
         if (!attribute.empty()) {
            std::vector<std::string> strs;
            boost::split(strs, std::string(attribute.value()), boost::is_any_of("#"),
                         boost::token_compress_on);
            prop_type = strs.back();

            if (prop_type == "Element" || prop_type == "BPMNDiagram") {  // Temporary treatment
               break;
            }

            if (prop_type == "String" || prop_type == "Boolean" || prop_type == "Integer") {
               if (prop_type == "String") {
                  prop_type = "std::string";
                  rawCppHeaders.emplace("<string>");
               } else if (prop_type == "Boolean") {
                  prop_type = "bool";
               } else {
                  prop_type = "int";
               }

               is_primitive = true;
            } else {
               auto it = std::find_if(data.begin(), data.end(), [&](inja::json json) {
                  return json["enum_name"] == prop_type;
               });

               if (it != data.end()) {
                  rawForwardEnumDcls.insert(prop_type);
               } else {
                  rawForwardDcls.insert(prop_type);
               }
            }
         }
      } else {
         auto it = std::find_if(data.begin(), data.end(),
                                [&](inja::json json) { return json["enum_name"] == prop_type; });

         if (it != data.end()) {
            rawForwardEnumDcls.insert(prop_type);
         } else {
            rawForwardDcls.insert(prop_type);
         }
      }

      if (!prop_cardinality.empty()) {
         rawCppHeaders.emplace("<algorithm>");
         rawCppHeaders.emplace("<memory>");
         rawCppHeaders.emplace("<utility>");
         rawCppHeaders.emplace("<vector>");
      }

      if (!is_primitive) {
         rawCppHeaders.emplace("<memory>");
         rawCppHeaders.emplace("<utility>");
      }

      jsonProperties.push_back({ { "property_name", prop_name },
                                 { "property_type", prop_type },
                                 { "data_type", (is_primitive) ? "primitive" : "user-defined" },
                                 { "isComposite", prop_composite },
                                 { "property_cardinality", prop_cardinality } });
   }

   auto it = std::find_if(data.begin(), data.end(), [&](inja::json json) {
      return json["class_name"] == classNode.attribute("name").value();
   });

   it->emplace("attributes", jsonProperties);

   std::copy(rawCppHeaders.begin(), rawCppHeaders.end(), std::back_inserter(jsonCppHeaders));

   it->emplace("cpp_headers", jsonCppHeaders);

   std::copy(rawForwardEnumDcls.begin(), rawForwardEnumDcls.end(),
             std::back_inserter(jsonForwardEnumDcls));

   it->emplace("forward_enum_dcls", jsonForwardEnumDcls);

   for (std::string base : (*it)["base_class"]) {
      base.erase(std::remove(base.begin(), base.end(), '\"'), base.end());
      rawBaseClasses.insert(base);
   }

   std::copy_if(rawForwardDcls.begin(), rawForwardDcls.end(), std::back_inserter(jsonForwardDcls),
                [&](const auto &rawForwardDcl) {
                   return std::find(rawBaseClasses.begin(), rawBaseClasses.end(), rawForwardDcl)
                          == rawBaseClasses.end();
                });

   it->emplace("forward_dcls", jsonForwardDcls);
}
