#!/bin/bash

docker build -t sandroandrade/cpp-static-analyzers:latest .
docker login -u sandroandrade
docker push sandroandrade/cpp-static-analyzers:latest
