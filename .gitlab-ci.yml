image: sandroandrade/cpp-static-analyzers

variables:
  SONAR_USER_HOME: "${CI_PROJECT_DIR}/.sonar"  # Defines the location of the analysis task cache
  GIT_DEPTH: "0"  # Tells git to fetch all the branches of the project, required by the analysis task

get-sonar-binaries:
  stage: .pre
  tags:
    - ifba
    - docker
  cache:
    policy: push
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build-wrapper/
      - sonar-scanner/
  script:
    # Download sonar-scanner
    - curl -sSLo ./sonar-scanner.zip 'https://binaries.sonarsource.com/Distribution/sonar-scanner-cli/sonar-scanner-cli-4.4.0.2170-linux.zip'
    - unzip -o sonar-scanner.zip
    - mv sonar-scanner-4.4.0.2170-linux sonar-scanner
    # Download build-wrapper
    - curl -sSLo ./build-wrapper-linux-x86.zip "${SONAR_HOST_URL}/static/cpp/build-wrapper-linux-x86.zip"
    - unzip -oj build-wrapper-linux-x86.zip -d ./build-wrapper
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"' # Run sonar job in merge request pipelines
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'      # Run sonar job in pipelines on the master branch (but not in other branch pipelines)
    - if: '$CI_COMMIT_TAG'                               # Run sonar job in pipelines for tags

build:
  stage: build
  tags:
    - ifba
    - docker
  cache:
    policy: pull-push
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build-wrapper/
      - sonar-scanner/
      - bw-output/
      - build/
  # install the necessary build tools when needed
  script:
    # Run the build inside the build wrapper
    - mkdir build
    - cd build
    - cmake -GNinja -DCMAKE_EXPORT_COMPILE_COMMANDS=ON -DCMAKE_LINK_WHAT_YOU_USE=True ..
    - cd ..
    - build-wrapper/build-wrapper-linux-x86-64 --out-dir bw-output cmake --build build/ --config Release
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"' # Run build job in merge request pipelines
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'      # Run build job in pipelines on the master branch (but not in other branch pipelines)
    - if: '$CI_COMMIT_TAG'                               # Run build job in pipelines for tags

static-analyzers:
  stage: test
  tags:
    - ifba
    - docker
  cache:
    policy: pull-push
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build/
      - sonar-scanner/
      - bw-output/
  script:
    - scripts/run-analyzers.sh
  artifacts:
    reports:
      codequality: final-quality-report.json
    paths: [final-quality-report.json, iwyu-full.txt]
    expire_in: 1 week
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"' # Run static analyzers in merge request pipelines
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'      # Run static analyzers in pipelines on the master branch (but not in other branch pipelines)
    - if: '$CI_COMMIT_TAG'                               # Run static analyzers in pipelines for tags

sonarcloud-check:
  stage: .post
  tags:
    - ifba
    - docker
  cache:
    policy: pull
    key: "${CI_COMMIT_SHORT_SHA}"
    paths:
      - build-wrapper/
      - sonar-scanner/
      - bw-output/
      - build/
  script:
    - sonar-scanner/bin/sonar-scanner -Dsonar.host.url="${SONAR_HOST_URL}" -Dsonar.token="${SONAR_TOKEN}" -Dsonar.cfamily.build-wrapper-output=bw-output
  rules:
    - if: '$CI_PIPELINE_SOURCE == "merge_request_event"' # Run sonar job in merge request pipelines
    - if: '$CI_COMMIT_BRANCH == $CI_DEFAULT_BRANCH'      # Run sonar job in pipelines on the master branch (but not in other branch pipelines)
    - if: '$CI_COMMIT_TAG'                               # Run sonar job in pipelines for tags
